Create a database - DONE

Create routes - @todo

Create policies - @todo

Create models - @todo

Add the cards
    - import the cards (manually) - DONE
    - set up an update job for new cards https://duelistsunite.org/omega/OmegaDB.cdb - to be tested

Create a deck builder interface
    - basic name search - DONE (search), @todo click to add
    - ydk import
    - ydke import
    - omega code import

Create a bot-starter builder interface
    - card selector - DONE (search), @todo click to add
    - card location selector - @todo get location from type

Create a bot-action builder interface
    - card selector - DONE (search), @todo click to add
    - card actions options

Create a bot builder interface
    - deck selector
    - first/second checkbox
    - default reposition checkbox
    - starters multiple selector
    - actions multiple selector

Create a bot exporter
    - export to windbot [edopro, omega]
    - export to bot api [omega]

Create a replay importer
    - import replays from edopro
    - import replays from omega
