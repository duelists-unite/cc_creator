#!/bin/bash

cd /var/www

rm -rf /var/www/laradock

git clone https://gitlab.com/duelists-unite/cc_creator.git .

chown laradock:laradock /var/www

composer install

cp .env.develop .env

php artisan optimize

php artisan migrate

php artisan cards:update
