<?php

namespace App\Enums;

use ReflectionClass;

class Enum
{
    public static function toArray(): array
    {
        return (new ReflectionClass(get_called_class()))->getConstants();
    }

    public static function toString(string $glue = ','): string
    {
        return implode($glue, self::toArray());
    }
}
