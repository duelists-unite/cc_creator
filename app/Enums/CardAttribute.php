<?php

namespace App\Enums;

final class CardAttribute extends Enum
{
    public const EARTH = 'earth';
    public const WATER = 'water';
    public const FIRE = 'fire';
    public const WIND = 'wind';
    public const LIGHT = 'light';
    public const DARK = 'dark';
    public const DIVINE = 'divine';

    public static function map(int $flag, string $glue = ',')
    {
        $attributes = collect();

        for ($bit = 1; $bit <= $flag; $bit *= 2) {
            if ($flag & $bit) {
                switch ($bit) {
                    case 0x01: $attributes[] = self::EARTH; break;
                    case 0x02: $attributes[] = self::WATER; break;
                    case 0x04: $attributes[] = self::FIRE; break;
                    case 0x08: $attributes[] = self::WIND; break;
                    case 0x10: $attributes[] = self::LIGHT; break;
                    case 0x20: $attributes[] = self::DARK; break;
                    case 0x40: $attributes[] = self::DIVINE; break;
                }
            }
        }

        return $attributes;
    }

    public static function encode(array $attributes)
    {
        $map = [
            self::EARTH => 0x01,
            self::WATER => 0x02,
            self::FIRE => 0x04,
            self::WIND => 0x08,
            self::LIGHT => 0x10,
            self::DARK => 0x20,
            self::DIVINE => 0x40
        ];

        $flag = 0;

        foreach ($attributes as $attribute) {
            $flag = $flag | $map[$attribute];
        }

        return $flag;
    }
}
