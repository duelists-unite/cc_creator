<?php

namespace App\Enums;

final class CardGenre extends Enum
{
    public const DestroySpellTrap = 'destroy spell/trap';
    public const DestroyMonster = 'destroy monster';
    public const Banish = 'banish';
    public const SendtoGraveyard = 'send to gy';
    public const ReturntoHand = 'return to hand';
    public const ReturntoDeck = 'return to deck';
    public const DestroyHand = 'destroy hand';
    public const DestroyDeck = 'destroy deck';
    public const HelpDraw = 'help draw';
    public const SearchDeck = 'search deck';
    public const RecoverfromGY = 'recover from gy';
    public const ChangePosition = 'change position';
    public const ChangeControl = 'change control';
    public const ChangeATKDEF = 'change atk/def';
    public const Piercing = 'piercing';
    public const AttackMultiple = 'multiple atk';
    public const LimitAttack = 'limit atk';
    public const DirectAttack = 'direct atk';
    public const SpecialSummon = 'special summon';
    public const Token = 'token';
    public const TypeRelated = 'type related';
    public const AttributeRelated = 'attribute related';
    public const DamageLP = 'damage lp';
    public const GainLP = 'gain lp';
    public const Cannotbedestroyed = 'indestructible';
    public const CannotSummon = 'cannot summon';
    public const Counter = 'counter';
    public const Gamble = 'gamble';
    public const FusionRelated = 'fusion related';
    public const SynchroRelated = 'synchro related';
    public const XyzRelated = 'xyz related';
    public const NegateEffect = 'negate';
    public const RitualRelated = 'ritual related';
    public const PendulumRelated = 'pend related';
    public const LinkRelated = 'link related';
    public const HandTraps = 'hand trap';

    public static function map(int $flag, string $glue = ',')
    {
        $genres = collect();

        for ($bit = 1; $bit <= $flag; $bit *= 2) {
            if ($flag & $bit) {
                switch ($bit) {
                    case 0x01: $genres[] = self::DestroySpellTrap; break;
                    case 0x02: $genres[] = self::DestroyMonster; break;
                    case 0x04: $genres[] = self::Banish; break;
                    case 0x08: $genres[] = self::SendtoGraveyard; break;
                    case 0x10: $genres[] = self::ReturntoHand; break;
                    case 0x20: $genres[] = self::ReturntoDeck; break;
                    case 0x40: $genres[] = self::DestroyHand; break;
                    case 0x80: $genres[] = self::DestroyDeck; break;
                    case 0x100: $genres[] = self::HelpDraw; break;
                    case 0x200: $genres[] = self::SearchDeck; break;
                    case 0x400: $genres[] = self::RecoverfromGY; break;
                    case 0x800: $genres[] = self::ChangePosition; break;
                    case 0x1000: $genres[] = self::ChangeControl; break;
                    case 0x2000: $genres[] = self::ChangeATKDEF; break;
                    case 0x4000: $genres[] = self::Piercing; break;
                    case 0x8000: $genres[] = self::AttackMultiple; break;
                    case 0x10000: $genres[] = self::LimitAttack; break;
                    case 0x20000: $genres[] = self::DirectAttack; break;
                    case 0x40000: $genres[] = self::SpecialSummon; break;
                    case 0x80000: $genres[] = self::Token; break;
                    case 0x100000: $genres[] = self::TypeRelated; break;
                    case 0x200000: $genres[] = self::AttributeRelated; break;
                    case 0x400000: $genres[] = self::DamageLP; break;
                    case 0x800000: $genres[] = self::GainLP; break;
                    case 0x1000000: $genres[] = self::Cannotbedestroyed; break;
                    case 0x2000000: $genres[] = self::CannotSummon; break;
                    case 0x4000000: $genres[] = self::Counter; break;
                    case 0x8000000: $genres[] = self::Gamble; break;
                    case 0x10000000: $genres[] = self::FusionRelated; break;
                    case 0x20000000: $genres[] = self::SynchroRelated; break;
                    case 0x40000000: $genres[] = self::XyzRelated; break;
                    case 0x80000000: $genres[] = self::NegateEffect; break;
                    case 0x100000000: $genres[] = self::RitualRelated; break;
                    case 0x200000000: $genres[] = self::PendulumRelated; break;
                    case 0x400000000: $genres[] = self::LinkRelated; break;
                    case 0x800000000: $genres[] = self::HandTraps; break;
                }
            }
        }

        return $genres;
    }

    public static function encode(array $genres)
    {
        $map = [
            self::DestroySpellTrap => 0x01,
            self::DestroyMonster => 0x02,
            self::Banish => 0x04,
            self::SendtoGraveyard => 0x08,
            self::ReturntoHand => 0x10,
            self::ReturntoDeck => 0x20,
            self::DestroyHand => 0x40,
            self::DestroyDeck => 0x80,
            self::HelpDraw => 0x100,
            self::SearchDeck => 0x200,
            self::RecoverfromGY => 0x400,
            self::ChangePosition => 0x800,
            self::ChangeControl => 0x1000,
            self::ChangeATKDEF => 0x2000,
            self::Piercing => 0x4000,
            self::AttackMultiple => 0x8000,
            self::LimitAttack => 0x10000,
            self::DirectAttack => 0x20000,
            self::SpecialSummon => 0x40000,
            self::Token => 0x80000,
            self::TypeRelated => 0x100000,
            self::AttributeRelated => 0x200000,
            self::DamageLP => 0x400000,
            self::GainLP => 0x800000,
            self::Cannotbedestroyed => 0x1000000,
            self::CannotSummon => 0x2000000,
            self::Counter => 0x4000000,
            self::Gamble => 0x8000000,
            self::FusionRelated => 0x10000000,
            self::SynchroRelated => 0x20000000,
            self::XyzRelated => 0x40000000,
            self::NegateEffect => 0x80000000,
            self::RitualRelated => 0x100000000,
            self::PendulumRelated => 0x200000000,
            self::LinkRelated => 0x400000000,
            self::HandTraps => 0x800000000
        ];

        $flag = 0;

        foreach ($genres as $genre) {
            $flag = $flag | $map[$genre];
        }

        return $flag;
    }
}
