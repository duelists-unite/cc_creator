<?php

namespace App\Enums;

final class CardRace extends Enum
{
    public const WARRIOR = 'warrior';
    public const SPELLCASTER = 'spell caster';
    public const FAIRY = 'fairy';
    public const FIEND = 'fiend';
    public const ZOMBIE = 'zombie';
    public const MACHINE = 'machine';
    public const AQUA = 'aqua';
    public const PYRO = 'pyro';
    public const ROCK = 'rock';
    public const WINGEDBEAST = 'winged beast';
    public const PLANT = 'plant';
    public const INSECT = 'insect';
    public const THUNDER = 'thunder';
    public const DRAGON = 'dragon';
    public const BEAST = 'beast';
    public const BEASTWARRIOR = 'beast warrior';
    public const DINOSAUR = 'dinosaur';
    public const FISH = 'fish';
    public const SEASERPENT = 'sea serpent';
    public const REPTILE = 'reptile';
    public const PSYCHIC = 'psychic';
    public const DIVINEBEAST = 'divine beast';
    public const CREATORGOD = 'creator god';
    public const WYRM = 'wyrm';
    public const CYBERSE = 'cyberse';

    public static function map(int $flag, string $glue = ',')
    {
        $races = collect();

        for ($bit = 1; $bit <= $flag; $bit *= 2) {
            if ($flag & $bit) {
                switch ($bit) {
                    case 0x1:       $races[] = self::WARRIOR; break;
                    case 0x2:       $races[] = self::SPELLCASTER; break;
                    case 0x4:       $races[] = self::FAIRY; break;
                    case 0x8:       $races[] = self::FIEND; break;
                    case 0x10:      $races[] = self::ZOMBIE; break;
                    case 0x20:      $races[] = self::MACHINE; break;
                    case 0x40:      $races[] = self::AQUA; break;
                    case 0x80:      $races[] = self::PYRO; break;
                    case 0x100:     $races[] = self::ROCK; break;
                    case 0x200:     $races[] = self::WINGEDBEAST; break;
                    case 0x400:     $races[] = self::PLANT; break;
                    case 0x800:     $races[] = self::INSECT; break;
                    case 0x1000:    $races[] = self::THUNDER; break;
                    case 0x2000:    $races[] = self::DRAGON; break;
                    case 0x4000:    $races[] = self::BEAST; break;
                    case 0x8000:    $races[] = self::BEASTWARRIOR; break;
                    case 0x10000:   $races[] = self::DINOSAUR; break;
                    case 0x20000:   $races[] = self::FISH; break;
                    case 0x40000:   $races[] = self::SEASERPENT; break;
                    case 0x80000:   $races[] = self::REPTILE; break;
                    case 0x100000:  $races[] = self::PSYCHIC; break;
                    case 0x200000:  $races[] = self::DIVINEBEAST; break;
                    case 0x400000:  $races[] = self::CREATORGOD; break;
                    case 0x800000:  $races[] = self::WYRM; break;
                    case 0x1000000: $races[] = self::CYBERSE; break;
                }
            }
        }

        return $races;
    }

    public static function encode(array $races): int
    {
        $map = [
            self::WARRIOR => 0x1,
            self::SPELLCASTER => 0x2,
            self::FAIRY => 0x4,
            self::FIEND => 0x8,
            self::ZOMBIE => 0x10,
            self::MACHINE => 0x20,
            self::AQUA => 0x40,
            self::PYRO => 0x80,
            self::ROCK => 0x100,
            self::WINGEDBEAST => 0x200,
            self::PLANT => 0x400,
            self::INSECT => 0x800,
            self::THUNDER => 0x1000,
            self::DRAGON => 0x2000,
            self::BEAST => 0x4000,
            self::BEASTWARRIOR => 0x8000,
            self::DINOSAUR => 0x10000,
            self::FISH => 0x20000,
            self::SEASERPENT => 0x40000,
            self::REPTILE => 0x80000,
            self::PSYCHIC => 0x100000,
            self::DIVINEBEAST => 0x200000,
            self::CREATORGOD => 0x400000,
            self::WYRM => 0x800000,
            self::CYBERSE => 0x1000000
        ];

        $flag = 0;

        foreach ($races as $race) {
            $flag = $flag | $map[$race];
        }

        return $flag;
    }
}
