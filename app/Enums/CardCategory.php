<?php

namespace App\Enums;

final class CardCategory extends Enum
{
    public const Skill = 'skill';
    public const SpeedSpell = 'speed spell';
    public const Boss = 'boss';
    public const Beta = 'beta';
    public const Action = 'action';
    public const Command = 'command';
    public const DoubleScript = 'double script';
    public const RushLegendary = 'rush legendary';
    public const PreErrata = 'pre errata';
    public const Dark = 'dark card';
    public const DuelLinks = 'duel links';
    public const Rush = 'rush';
    public const Start = 'start';
    public const OneCard = 'one card';
    public const TwoCard = 'two card';
    public const ThreeCard = 'three card';
    public const LevelZero = 'level zero';
    public const TreatedAs = 'treated as';
    public const BlueGod = 'blue god';
    public const YellowGod = 'yellow god';
    public const RedGod = 'red god';
    public const RushMax = 'rush max';
    public const SC = 'simplified chinese';
    public const PendulumEffect = 'pendulum';

    public static function map(int $flag, string $glue = ',')
    {
        $categories = collect();

        for ($bit = 1; $bit <= $flag; $bit *= 2) {
            if ($flag & $bit) {
                switch ($bit) {
                    case 0x01: $categories[] = self::Skill; break;
                    case 0x02: $categories[] = self::SpeedSpell; break;
                    case 0x04: $categories[] = self::Boss; break;
                    case 0x08: $categories[] = self::Beta; break;
                    case 0x10: $categories[] = self::Action; break;
                    case 0x20: $categories[] = self::Command; break;
                    case 0x40: $categories[] = self::DoubleScript; break;
                    case 0x80: $categories[] = self::RushLegendary; break;
                    case 0x100: $categories[] = self::PreErrata; break;
                    case 0x200: $categories[] = self::Dark; break;
                    case 0x400: $categories[] = self::DuelLinks; break;
                    case 0x800: $categories[] = self::Rush; break;
                    case 0x1000: $categories[] = self::Start; break;
                    case 0x2000: $categories[] = self::OneCard; break;
                    case 0x4000: $categories[] = self::TwoCard; break;
                    case 0x8000: $categories[] = self::ThreeCard; break;
                    case 0x10000: $categories[] = self::LevelZero; break;
                    case 0x20000: $categories[] = self::TreatedAs; break;
                    case 0x40000: $categories[] = self::BlueGod; break;
                    case 0x80000: $categories[] = self::YellowGod; break;
                    case 0x100000: $categories[] = self::RedGod; break;
                    case 0x200000: $categories[] = self::RushMax; break;
                    case 0x400000: $categories[] = self::SC; break;
                    case 0x800000: $categories[] = self::PendulumEffect; break;
                }
            }
        }

        return $categories;
    }

    public static function encode(array $categories)
    {
        $map = [
            self::Skill => 0x01,
            self::SpeedSpell => 0x02,
            self::Boss => 0x04,
            self::Beta => 0x08,
            self::Action => 0x10,
            self::Command => 0x20,
            self::DoubleScript => 0x40,
            self::RushLegendary => 0x80,
            self::PreErrata => 0x100,
            self::Dark => 0x200,
            self::DuelLinks => 0x400,
            self::Rush => 0x800,
            self::Start => 0x1000,
            self::OneCard => 0x2000,
            self::TwoCard => 0x4000,
            self::ThreeCard => 0x8000,
            self::LevelZero => 0x10000,
            self::TreatedAs => 0x20000,
            self::BlueGod => 0x40000,
            self::YellowGod => 0x80000,
            self::RedGod => 0x100000,
            self::RushMax => 0x200000,
            self::SC => 0x400000,
            self::PendulumEffect => 0x800000
        ];

        $flag = 0;

        foreach ($categories as $category)
        {
            $flag = $flag | $map[$category];
        }

        return $flag;
    }
}
