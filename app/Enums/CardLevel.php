<?php

namespace App\Enums;

final class CardLevel extends Enum
{
    public static function leftScale($level): int
    {
        return ($level & 0xFF000000) >> 24;
    }

    public static function rightScale($level): int
    {
        return ($level & 0x00FF0000) >> 16;
    }

    public static function level($level): int
    {
        return ($level & 0x0000FFFF);
    }

    public static function scales($level): array
    {
        return [
            'left' => self::leftScale($level),
            'right' => self::rightScale($level)
        ];
    }

    public static function encode($level, $right, $left, int $type): int
    {
        if (!($type & 0x1000000)) {
            return $level; // if card type is not PENDULUM -> level => value
        }

        return ($right << 16) + ($left << 24) + $level;
    }
}
