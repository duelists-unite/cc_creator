<?php

namespace App\Enums;

final class CardType extends Enum
{
    public const NONE = null;
    public const MONSTER = 'monster';
    public const SPELL = 'spell';
    public const TRAP = 'trap';
    public const NORMAL = 'normal';
    public const FUSION = 'fusion';
    public const RITUAL = 'ritual';
    public const TRAPMONSTER = 'trap monster';
    public const SPIRIT = 'spirit';
    public const UNION = 'union';
    public const GEMINI = 'gemini';
    public const TUNER = 'tuner';
    public const SYNCHRO = 'synchro';
    public const TOKEN = 'token';
    public const QUICKPLAY = 'quick play';
    public const CONTINUOUS = 'continuous';
    public const EQUIP = 'equip';
    public const FIELD = 'field';
    public const COUNTER = 'counter';
    public const FLIP = 'flip';
    public const TOON = 'toon';
    public const XYZ = 'xyz';
    public const PENDULUM = 'pendulum';
    public const SPSUMMON = 'summon';   //cards that cannot be normal summoned or set and
                                        // must (first) be special summoned by ...
    public const LINK = 'link';
    public const EFFECT = 'effect';

    public static function map(int $flag, string $glue = ',')
    {
        $types = collect();

        for ($bit = 1; $bit <= $flag; $bit *= 2) {
            if ($flag & $bit) {
                switch ($bit) {
                    case 0x0:       break;
                    case 0x1:       $types[] = self::MONSTER; break;
                    case 0x2:       $types[] = self::SPELL; break;
                    case 0x4:       $types[] = self::TRAP; break;
                    case 0x10:      $types[] = self::NORMAL; break;
                    case 0x20:      $types[] = self::EFFECT; break;
                    case 0x40:      $types[] = self::FUSION; break;
                    case 0x80:      $types[] = self::RITUAL; break;
                    case 0x100:     $types[] = self::TRAPMONSTER; break;
                    case 0x200:     $types[] = self::SPIRIT; break;
                    case 0x400:     $types[] = self::UNION; break;
                    case 0x800:     $types[] = self::GEMINI; break;
                    case 0x1000:    $types[] = self::TUNER; break;
                    case 0x2000:    $types[] = self::SYNCHRO; break;
                    case 0x4000:    $types[] = self::TOKEN; break;
                    case 0x10000:   $types[] = self::QUICKPLAY; break;
                    case 0x20000:   $types[] = self::CONTINUOUS; break;
                    case 0x40000:   $types[] = self::EQUIP; break;
                    case 0x80000:   $types[] = self::FIELD; break;
                    case 0x100000:  $types[] = self::COUNTER; break;
                    case 0x200000:  $types[] = self::FLIP; break;
                    case 0x400000:  $types[] = self::TOON; break;
                    case 0x800000:  $types[] = self::XYZ; break;
                    case 0x1000000: $types[] = self::PENDULUM; break;
                    case 0x2000000: $types[] = self::SPSUMMON; break;
                    case 0x4000000: $types[] = self::LINK; break;
                }
            }
        }

        return $types;
    }

    public static function encode(array $types)
    {
        $map = [
            self::NONE => 0x0,
            self::MONSTER => 0x1,
            self::SPELL => 0x2,
            self::TRAP => 0x4,
            // 0x8
            self::NORMAL => 0x10,
            self::EFFECT => 0x20,
            self::FUSION => 0x40,
            self::RITUAL => 0x80,
            self::TRAPMONSTER => 0x100,
            self::SPIRIT => 0x200,
            self::UNION => 0x400,
            self::GEMINI => 0x800,
            self::TUNER => 0x1000,
            self::SYNCHRO => 0x2000,
            self::TOKEN => 0x4000,
            self::QUICKPLAY => 0x10000,
            self::CONTINUOUS => 0x20000,
            self::EQUIP => 0x40000,
            self::FIELD => 0x80000,
            self::COUNTER => 0x100000,
            self::FLIP => 0x200000,
            self::TOON => 0x400000,
            self::XYZ => 0x800000,
            self::PENDULUM => 0x1000000,
            self::SPSUMMON => 0x2000000,
            self::LINK => 0x4000000
        ];

        $flag = 0;

        foreach ($types as $type) {
            $flag = $flag | $map[$type];
        }

        return $flag;
    }
}
