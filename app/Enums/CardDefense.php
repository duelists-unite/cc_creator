<?php

namespace App\Enums;

final class CardDefense extends Enum
{
    public const LEFT = 'l';
    public const RIGHT = 'r';
    public const TOP = 't';
    public const BOTTOM = 'b';
    public const TOP_RIGHT = 'tr';
    public const TOP_LEFT = 'tl';
    public const BOTTOM_LEFT = 'bl';
    public const BOTTOM_RIGHT = 'br';

    public static function markers(int $flag, int $type, string $glue = ',')
    {
        $markers = collect();

        if (!($type & 0x4000000)) {
            return null; // if card type is not LINK -> markers => null
        }

        for ($bit = 1; $bit <= $flag; $bit *= 2) {
            if ($flag & $bit) {
                switch ($bit) {
                    case 0x01: $markers[] = self::BOTTOM_LEFT; break;
                    case 0x02: $markers[] = self::BOTTOM; break;
                    case 0x04: $markers[] = self::BOTTOM_RIGHT; break;
                    case 0x08: $markers[] = self::LEFT; break;
                    case 0x20: $markers[] = self::RIGHT; break;
                    case 0x40: $markers[] = self::TOP_LEFT; break;
                    case 0x80: $markers[] = self::TOP; break;
                    case 0x100: $markers[] = self::TOP_RIGHT; break;
                }
            }
        }

        return $markers;
    }

    public static function def(int $flag, int $type): int
    {
        if (!($type & 0x4000000)) {
            return $flag; // if card type is not LINK -> def => value
        }

        return 0;
    }

    public static function encode(int $def, array $markers, int $type): int
    {
        if (!($type & 0x4000000)) {
            return $def; // if card type is not LINK -> def => value
        }

        $map = [
            self::TOP => 0x80,
            self::TOP_RIGHT => 0x100,
            self::RIGHT => 0x20,
            self::BOTTOM_RIGHT => 0x04,
            self::BOTTOM => 0x02,
            self::BOTTOM_LEFT => 0x01,
            self::LEFT => 0x08,
            self::TOP_LEFT => 0x40,
        ];

        $flag = 0;

        foreach ($markers as $marker) {
            $flag = $flag | $map[$marker];
        }

        return $flag;
    }
}
