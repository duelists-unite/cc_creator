<?php

namespace App\Enums;

final class CardLocationsInDeck extends Enum
{
    public const MAIN  = 1;
    public const EXTRA = 2;
    public const SIDE  = 3;
}
