<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class Card extends Model
{
    use HasFactory, Searchable, SoftDeletes;

    protected $fillable = [
        'card_id',
        'status',
        'name',
        'description',
        'strings',
        'types',
        'atk',
        'def',
        'level',
        'race',
        'attribute',
        'categories',
        'genres',
        'ocg_date',
        'tcg_date',
        'pend_scales',
        'link_markers',
//        'color'
    ];

    protected $casts = [
        'strings' => 'json',
        'types' => 'json',
        'race' => 'json',
        'attribute' => 'json',
        'categories' => 'json',
        'genres' => 'json',
        'pend_scales' => 'json',
        'link_markers' => 'json',
        'ocg_date' => 'timestamp',
        'tcg_date' => 'timestamp',
    ];

    public function toSearchableArray()
    {
        $array = $this->only([
            'card_id',
            'name',
            'description',
            'types',
            'atk',
            'def',
            'level',
            'race',
            'attribute',
            'categories',
            'genres',
            'ocg_date',
            'tcg_date',
            'pend_scales',
            'link_markers',
        ]);



        return $array;
    }

//    public function getTypeAttribute($type)
//    {
//        return explode(',', $type);
//    }
//
//    public function getRaceAttribute($race)
//    {
//        return explode(',', $race);
//    }
//
//    public function getAttributeAttribute($attribute)
//    {
//        return explode(',', $attribute);
//    }
}
