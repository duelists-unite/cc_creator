<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Collection extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'type',
        'items',
        'count',
        'next_id',
        'available_until'
    ];

    protected $hidden = [
        'next_id',
        'items',
        'created_at',
        'updated_at',
        'available_until'
    ];

    protected $appends = [
        'url'
    ];

    protected $casts = [
        'available_until' => 'datetime',
    ];

    public function getUrlAttribute(): string
    {
        return route('collection.download', ['id' => $this->id]);
    }

    public function setItemsAttribute($item)
    {
        if (empty($this->attributes['items'])) {
            $this->attributes['items'] = $item;
        } else {
            $items = explode(',', $this->attributes['items']);
            $deleted = false;

            /**
             * if $item exists - remove it
             * else - add it
             */
            foreach ($items as $index => $id) {
                if ($id == $item) {
                    $deleted = true;
                    unset($items[$index]);
                    break;
                }
            }

            if (!$deleted) {
                $items[] = $item;
            }

            $this->attributes['items'] = implode(',', $items);
        }
    }
}
