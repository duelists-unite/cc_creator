<?php

namespace App\Models\Omega;


class Set extends Model
{
    protected $table = 'setcodes';

    protected $fillable = [
        'officialcode',
        'betacode',
        'name',
        'cardid'
    ];
}
