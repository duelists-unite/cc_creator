<?php

namespace App\Models\Omega;

class Text extends Model
{
    protected $fillable = [
        'name',
        'desc',
        'str1',
        'str2',
        'str3',
        'str4',
        'str5',
        'str6',
        'str7',
        'str8',
        'str9',
        'str10',
        'str11',
        'str12',
        'str13',
        'str14',
        'str15',
        'str16',
    ];

    public function getStringsAttribute()
    {
        return $this->only([
            'str1',
            'str2',
            'str3',
            'str4',
            'str5',
            'str6',
            'str7',
            'str8',
            'str9',
            'str10',
            'str11',
            'str12',
            'str13',
            'str14',
            'str15',
            'str16',
        ]);
    }
}
