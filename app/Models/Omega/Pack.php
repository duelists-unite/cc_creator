<?php

namespace App\Models\Omega;


class Pack extends Model
{
    protected $fillable = [
        'abbr',
        'name',
        'ocgdate',
        'tcgdate'
    ];

    protected $casts = [
        'ocgdate' => 'timestamp',
        'tcgdate' => 'timestamp'
    ];
}
