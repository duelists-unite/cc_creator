<?php

namespace App\Models\Omega;

use \Illuminate\Database\Eloquent\Model as EloquentModel;

class Model extends EloquentModel
{
    protected $connection = 'omega_sqlite';
    public $timestamps = false;

}
