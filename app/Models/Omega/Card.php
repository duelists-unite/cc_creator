<?php

namespace App\Models\Omega;


use Illuminate\Database\Eloquent\Relations\HasOne;

class Card extends Model
{
    protected $table = 'datas';

    protected $fillable = [
        'id',
        'ot',
        'alias',
        'setcode',
        'type',
        'atk',
        'def',
        'level',
        'race',
        'attribute',
        'category',
        'genre',
        'ocgdate',
        'tcgdate'
    ];

    protected $casts = [
        'ocgdate' => 'timestamp',
        'tcgdate' => 'timestamp'
    ];

    public function text(): HasOne
    {
        return $this->hasOne(Text::class, 'id');
    }
}
