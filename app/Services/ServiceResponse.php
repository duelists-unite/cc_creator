<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

class ServiceResponse
{
    private bool $success;
    private ?string $message;
    private ?Model $model;
    private array $data;

    public function __construct(
        bool $success = false,
        ?string $message = null,
        ?Model $model = null,
        array $data = []
    ) {
        $this->success = $success;
        $this->message = $message;
        $this->model = $model;
        $this->data = $data;
    }

    public function toArray()
    {
        return [
            'success' => $this->success,
            'message' => $this->message,
            'model' => $this->model,
            'data' => $this->data
        ];
    }

    public function toJson()
    {
        return json_encode($this->toArray(), true);
    }

    public function success()
    {
        return $this->success;
    }
}
