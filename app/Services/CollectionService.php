<?php

namespace App\Services;

use App\Models\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class CollectionService
{
    public static function create(UploadedFile $uploadedFile): ServiceResponse
    {
        try {
            DB::beginTransaction();

            $filename = uniqid('collection_', true).".json";

            Storage::disk('public')->put($filename, $uploadedFile->get());

            $collection_data = json_decode($uploadedFile->get(), true);
            $item_count = count($collection_data);

            if ($item_count > 5000) {
                throw ValidationException::withMessages([
                    "Collection out of space"
                ]);
            }

            $items = [];

            foreach ($collection_data as $item) {
                $items[] = $item['id'];
            }

            $collection = Collection::create([
                'name' => $filename,
                'type' => "custom-cards",
                'items' => implode(',', $items),
                'count' => $item_count,
                'next_id' => end($collection_data)['id'] + 1,
                'available_until' => now()->addHours(6)
            ]);

            DB::commit();

            return new ServiceResponse(true, null, $collection);

        } catch (\Exception $exception) {
            Log::error("CollectionService@create: ".$exception->getMessage());
            Log::error($exception->getTraceAsString());

            DB::rollBack();

            return new ServiceResponse(false, "An error occurred!");
        }
    }
}
