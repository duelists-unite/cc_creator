<?php

namespace App\Services\Omega;

use App\Enums\CardAttribute;
use App\Enums\CardCategory;
use App\Enums\CardDefense;
use App\Enums\CardGenre;
use App\Enums\CardLevel;
use App\Enums\CardRace;
use App\Enums\CardType;
use App\Models\Collection;
use App\Models\Omega\Card as OmegaCard;
use App\Services\ServiceResponse;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class CardService
{
    public static function create(array $data): ServiceResponse
    {
        try {
            /**
             * if card does not have a collection get a card id from omegaDB
             * else get a card id from its collection
             */
            if (empty($data['collection'])) {
                $last = OmegaCard::where('id', '>', 400000000)
                    ->where('id', '<', 499999999)
                    ->orderBy('id', 'desc')
                    ->first(['id']);

                if (++$last->id > 499999999) {
                    throw ValidationException::withMessages([
                        "Next ID out of reach."
                    ]);
                }

                $next_id = $last->id;

            } else {
                $collection = Collection::where('name', $data['collection'])
                    ->firstOrFail();

                $next_id = $collection->next_id;
            }

            if (empty($data['ocg_date'])) {
                $data['ocg_date'] = today();
            }

            if (empty($data['tcg_date'])) {
                $data['tcg_date'] = today();
            }

            $type = CardType::encode($data['types']);

            $card = collect([
                'id' => $next_id,
                'ot' => 4,
                'alias' => 0,
                'setcode' => 0,
                'type' => $type,
                'atk' => $data['atk'],
                'def' => CardDefense::encode(
                    $data['def'],
                    $data['link_markers'] ?? [],
                    $type
                ),
                'level' => CardLevel::encode(
                    $data['level'],
                    isset($data['pend_scales']) ? $data['pend_scales']['right'] : 0,
                    isset($data['pend_scales']) ? $data['pend_scales']['left'] : 0,
                    $type
                ),
                'race' => CardRace::encode([$data['race']]),
                'attribute' => CardAttribute::encode([$data['attribute']]),
                'category' => CardCategory::encode(
                    empty($data['categories']) ?
                        [] : $data['categories']
                ),
                'genre' => CardGenre::encode(
                    empty($data['genres']) ?
                        [] : $data['genres']
                ),
                'script' => null,
                'support' => 0,
                'ocgdate' => strtotime($data['ocg_date']),
                'tcgdate' => strtotime($data['tcg_date'])
            ]);

            $text = collect([
                'id' => $card['id'],
                'name' => $data['name'],
                'desc' => $data['description'],
            ]);

            for ($i = 0; $i < 16; $i++) {
                $text['str'.($i + 1)] = empty($data["strings"][$i]) ? "" : $data['strings'][$i];
            }

            // join card with its relations
            $card['text'] = $text;
            unset($text);

            // if no collection create one now
            if (empty($data['collection'])) {
                $name = uniqid('collection_', true);

                $response = Storage::disk('public')
                    ->put("$name.json", json_encode([$card]));

                if (!$response) {
                    throw new Exception("Failed to create collection.");
                }

                $collection = Collection::create([
                    'name' => "$name.json",
                    'type' => "custom-cards",
                    'items' => $card['id'],
                    'count' => 1,
                    'next_id' => $card['id'] + 1,
                    'available_until' => now()->addHours(6)
                ]);

            } else {

                $collection_data = json_decode(
                    Storage::disk('public')->get($collection->name)
                    , true
                );

                $collection_data[] = $card;
                $response = Storage::disk('public')
                    ->put($collection->name, json_encode($collection_data));

                if ($response) {
                    $collection->update([
                        'items' => $card['id'],
                        'next_id' => $card['id'] + 1,
                        'count' => $collection->count + 1
                    ]);
                }
            }

            return new ServiceResponse(true, null, $collection, $card->toArray());

        } catch (ValidationException $exception) {

            return new ServiceResponse(false, $exception->validator->errors()->first());

        } catch(\Exception $exception) {
            Log::error("CardService@create: ".$exception->getMessage());
            Log::error($exception->getTraceAsString());

            return new ServiceResponse(false, "An error occurred!");
        }
    }

    public static function update(array $data, int $id): ServiceResponse
    {
        try {
            $collection = Collection::where('name', $data['collection'])
                ->firstOrFail();

            if (!in_array($id, explode(',', $collection->items))) {
                throw ValidationException::withMessages([
                    "Card $id not found in collection {$data['collection']}."
                ]);
            }

            $collection_data = json_decode(
                Storage::disk('public')->get($collection->name),
                true
            );

            if (!$collection_data) {
                throw ValidationException::withMessages([
                    "Collection is corrupted!"
                ]);
            }

            if (empty($data['ocg_date'])) {
                $data['ocg_date'] = today();
            }

            if (empty($data['tcg_date'])) {
                $data['tcg_date'] = today();
            }

            $card = [];

            foreach ($collection_data as $item) {
                if ($item['id'] == $id) {
                    $type = CardType::encode($data['types']);

                    $item = array_merge($item, [
                        'type' => $type,
                        'atk' => $data['atk'],
                        'def' => CardDefense::encode(
                            $data['def'],
                            $data['link_markers'] ?? [],
                            $type
                        ),
                        'level' => CardLevel::encode(
                            $data['level'],
                            isset($data['pend_scales']) ? $data['pend_scales']['right'] : 0,
                            isset($data['pend_scales']) ? $data['pend_scales']['left'] : 0,
                            $type
                        ),
                        'race' => CardRace::encode([$data['race']]),
                        'attribute' => CardAttribute::encode([$data['attribute']]),
                        'category' => CardCategory::encode(
                            empty($data['categories']) ?
                                [] : $data['categories']
                        ),
                        'genre' => CardGenre::encode(
                            empty($data['genres']) ?
                                [] : $data['genres']
                        ),
                        'ocgdate' => strtotime($data['ocg_date']),
                        'tcgdate' => strtotime($data['tcg_date']),
                    ]);

                    $item['text'] = array_merge($item['text'], [
                        'id' => $item['id'],
                        'name' => $data['name'],
                        'desc' => $data['description']
                    ]);

                    for ($i = 0; $i < 16; $i++) {
                        $item['text']['str'.($i + 1)] = empty($data["strings"][$i]) ? "" : $data['strings'][$i];
                    }

                    $card = $item;
                    break;
                }
            }

            Storage::disk('public')->put($collection->name, json_encode($collection_data));

            return new ServiceResponse(true, null, $collection, $card);

        } catch (ValidationException $exception) {

            return new ServiceResponse(false, $exception->validator->errors()->first());

        } catch (\Exception $exception) {
            Log::error("CardService@update: ".$exception->getMessage());
            Log::error($exception->getTraceAsString());

            return new ServiceResponse(false, "An error occurred!");
        }
    }

    public static function delete(string $collection, int $id): ServiceResponse
    {
        try {
            $collection = Collection::where('name', $collection)
                ->firstOrFail();

            if (!in_array($id, explode(',', $collection->items))) {
                throw ValidationException::withMessages([
                    "Card $id not found in collection $collection->name."
                ]);
            }

            $collection_data = json_decode(
                Storage::disk('public')->get($collection->name),
                true
            );

            if (!$collection_data) {
                throw ValidationException::withMessages([
                    "Collection is corrupted!"
                ]);
            }

            $card = [];

            foreach ($collection_data as $index => $item) {
                if ($item['id'] == $id) {
                    $card = $item; // return card data for the last time
                    unset($collection_data[$index]);
                    break;
                }
            }

            $collection->update([
                'items' => $id,
                'count' => $collection->count - 1
            ]);

            Storage::disk('public')->put($collection->name, json_encode($collection_data));

            return new ServiceResponse(true, null, $collection, $card);

        } catch (ValidationException $exception) {

            return new ServiceResponse(false, $exception->validator->errors()->first());

        } catch (\Exception $exception) {
            Log::error("CardService@delete: ".$exception->getMessage());
            Log::error($exception->getTraceAsString());

            return new ServiceResponse(false, "An error occurred!");
        }
    }

    public static function view(string $collection, int $id): ServiceResponse
    {
        try {
            $collection = Collection::where('name', $collection)
                ->firstOrFail();

            if (!in_array($id, explode(',', $collection->items))) {
                throw ValidationException::withMessages([
                    "Card $id not found in collection $collection->name."
                ]);
            }

            $collection_data = json_decode(
                Storage::disk('public')->get($collection->name),
                true
            );

            if (!$collection_data) {
                throw ValidationException::withMessages([
                    "Collection is corrupted!"
                ]);
            }

            $card = Arr::first($collection_data, function ($item, $key) use ($id) {
                return $item['id'] == $id;
            });

            return new ServiceResponse(true, null, $collection, $card);

        } catch (ValidationException $exception) {

            return new ServiceResponse(false, $exception->validator->errors()->first());

        } catch (\Exception $exception) {
            Log::error("CardService@view: ".$exception->getMessage());
            Log::error($exception->getTraceAsString());

            return new ServiceResponse(false, "An error occurred!");
        }
    }
}
