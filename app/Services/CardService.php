<?php

namespace App\Services;

use App\Models\Card;
use App\Models\Omega\Card as OmegaCard;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CardService
{
    public static function appySearch(array $data)
    {
        return Card::search($data['search'], function($index, $query, $options) use ($data) {
            $filters = [];

            self::optionalRangeFilter('atk', $filters, $data);
            self::optionalRangeFilter('def', $filters, $data);
            self::optionalRangeFilter('level', $filters, $data);

            if (!empty($data['race'])) {
                self::valueFilter('race', $filters, $data['race']);
            }

            if (!empty($data['attribute'])) {
                self::valueFilter('attribute', $filters, $data['attribute']);
            }

            self::rangeOnlyFilter('ocg_date', $filters, $data, 'before', 'after');
            self::rangeOnlyFilter('tcg_date', $filters, $data, 'before', 'after');

            if (!empty($data['types'])) {
                self::inListFilter('types', $filters, $data['types'], $data['types_operator']);
            }

            $options['filter'] = $filters;
            return $index->rawSearch($query, $options);
        })->simplePaginate($request->per_page ?? 5);
    }

    protected static function valueFilter(
        string $field,
        array &$filters,
        $value,
        string $operator = '='
    ) {
        $filters[] = "$field $operator $value";
    }

    protected static function optionalRangeFilter(
        string $field,
        array &$filters,
        array &$data,
        string $less = 'less',
        string $more = 'more',
        string $eq = 'eq'
    ) {
        if (!empty($data[$field][$eq])) {
            self::valueFilter($field, $filters, $data[$field][$eq]);
        } else {
            self::rangeOnlyFilter($field, $filters, $data, $less, $more);
        }
    }

    protected static function rangeOnlyFilter(
        string $field,
        array &$filters,
        array &$data,
        string $less = 'less',
        string $more = 'more'
    ) {
        if (!empty($data[$field][$less])) {
            self::valueFilter($field, $filters, $data[$field][$less], '<');
        }
        if (!empty($data[$field][$more])) {
            self::valueFilter($field, $filters, $data[$field][$more], '>');
        }
    }

    protected static function inListFilter(
        string $field,
        array &$filters,
        array $list,
        string $operator = 'and'
    ) {
       if ($operator === 'and') {
           foreach ($list as $value) {
               self::valueFilter($field, $filters, $value);
           }
       } elseif ($operator === 'or') {
           $filters[] = array_map(function ($item) use ($field) {
               return "$field = $item";
           }, $list);
       }
    }
}
