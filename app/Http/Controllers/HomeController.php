<?php

namespace App\Http\Controllers;

use App\Enums\CardType;
use App\Models\Card;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
//        if ($request->has('search')) {
//            $cards = Card::search($request->search)
//                ->take(10)->get();
//        } else {
//            $cards = Card::take(10)->get();
//        }

//        $cards->map(function (&$card) {
//            if (in_array(CardType::EFFECT, $card->type)) {
//                return $card['color'] = '0xCE5D25';
//            } else if (in_array(CardType::NORMAL, $card->type)) {
//                return $card['color'] = '0xCFAE6B';
//            } else if (in_array(CardType::SPELL, $card->type)) {
//                return $card['color'] = '0x1F8788';
//            } else if (in_array(CardType::TRAP, $card->type)) {
//                return $card['color'] = '0xAA186B';
//            } else if (in_array(CardType::RITUAL, $card->type)) {
//                return $card['color'] = '0x2F86D3';
//            } else if (in_array(CardType::LINK, $card->type)) {
//                return $card['color'] = '0x1767F0';
//            } else if (in_array(CardType::SYNCHRO, $card->type)) {
//                return $card['color'] = '0xEAF5F9';
//            } else if (in_array(CardType::XYZ, $card->type)) {
//                return $card['color'] = '0x030D16';
//            }
//
//            return $card['color'] = '0x1F8788';
//        });

        $cards = collect();

        return view('cards.index', compact('cards'));
    }
}
