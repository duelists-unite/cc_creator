<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CollectionStoreRequest;
use App\Models\Collection;
use App\Services\CollectionService;
use App\Services\ServiceResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class CollectionController extends Controller
{
    public function view(Request $request, int $id): JsonResponse
    {
        try {
            $collection = Collection::findOrFail($id);

            $response = new ServiceResponse(true, null, $collection);

            return response()->json($response->toArray());

        } catch (\Exception $exception) {
            $response = new ServiceResponse(false, "An error occurred!");

            return response()->json($response->toArray());
        }
    }

    public function download(Request $request, int $id): ?BinaryFileResponse
    {
        try {
            $collection = Collection::findOrFail($id);

            $collection = Storage::disk('public')->path($collection->name);

            return response()->download($collection);

        } catch (\Exception $exception) {
            return null;
        }
    }

    public function create(CollectionStoreRequest $request): JsonResponse
    {
        $collection = $request->file('collection');

        $response = CollectionService::create($collection);

        return response()->json($response->toArray());
    }
}
