<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CardDeleteRequest;
use App\Http\Requests\Api\CardStoreRequest;
use App\Http\Requests\Api\CardSearchRequest;
use App\Http\Requests\Api\CardUpdateRequest;
use App\Http\Requests\Api\CardViewRequest;
use App\Models\Card;
use App\Services\CardService;
use App\Services\Omega\CardService as OmegaCardService;
use App\Services\ServiceResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CardController extends Controller
{
    public function index(CardSearchRequest $request): JsonResponse
    {
        $cards = CardService::appySearch($request->validated());

        return response()->json($cards);
    }

    public function view(CardViewRequest $request, int $id): JsonResponse
    {
        $response = OmegaCardService::view($request->collection, $id);

        return response()->json($response->toArray());
    }

    public function create(CardStoreRequest $request): JsonResponse
    {
        $response = OmegaCardService::create($request->validated());

        return response()->json($response->toArray());
    }

    public function update(CardUpdateRequest $request, int $id): JsonResponse
    {
        $response = OmegaCardService::update($request->validated(), $id);

        return response()->json($response->toArray());
    }

    public function delete(CardDeleteRequest $request, int $id): JsonResponse
    {
        $response = OmegaCardService::delete($request->collection, $id);

        return response()->json($response->toArray());
    }
}
