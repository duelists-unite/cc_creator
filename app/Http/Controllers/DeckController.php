<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Deck;

class DeckController extends Controller
{
    public function index(Request $request)
    {
        $search = null;

        if (isset($request->search)) {
            $search = $request->search;
            $decks = Deck::search($request->search);
        } else {
            $decks = Deck::inRandomOrder();
        }

        $decks = $decks->paginate($request->has('search') ? 6 : 20);

        return view('cards.index', compact('decks', 'search'));
    }

    public function create()
    {
        $cards = null;
        return view('decks.store-edit', compact('cards'));
    }
}
