<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Card;

class CardController extends Controller
{
    public function index(Request $request)
    {
        $search = null;

        if (isset($request->search)) {
            $search = $request->search;
            $cards = Card::search($request->search);
        } else {
            $cards = Card::inRandomOrder();
        }

        $cards = $cards->simplePaginate(isset($request->search) ? 6 : 20);

        return view('cards.index', compact('cards', 'search'));
    }
}
