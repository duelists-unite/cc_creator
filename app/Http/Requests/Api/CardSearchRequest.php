<?php

namespace App\Http\Requests\Api;

use App\Enums\CardType;
use App\Enums\CardRace;
use App\Enums\CardAttribute;

class CardSearchRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // todo: ot, category, genre filters
        return [
            'search' => 'required|string',
            'per_page' => 'nullable|numeric|integer|min:1|max:20',
            'current_page' => 'nullable|numeric|integer|min:1',
//            'with' => 'nullable|array|max:1',
//            'with.*' => 'required|string',
            'types' => 'nullable|array|max:3',
            'types.*' => 'required|distinct|string|in:'.CardType::toString(),
            'types_operator' => 'required_with:types|string|in:and,or',
            'atk' => 'nullable|array|max:2',
            'atk.eq' => 'nullable|prohibits:atk.less,atk.more|numeric|integer|min:0',
            'atk.less' => 'nullable|numeric|integer|min:0',
            'atk.more' => 'nullable|numeric|integer|min:0',
            'def' => 'nullable|array|max:2',
            'def.eq' => 'nullable|prohibits:def.less,def.more|numeric|integer|min:0',
            'def.less' => 'nullable|numeric|integer|min:0',
            'def.more' => 'nullable|numeric|integer|min:0',
            'level' => 'nullable|array|max:2',
            'level.eq' => 'nullable|prohibits:level.less,level.more|numeric|integer|min:1|max:16',
            'level.less' => 'nullable|numeric|integer|min:1|max:16',
            'level.more' => 'nullable|numeric|integer|min:1|max:16',
            'race' => 'nullable|string|in:'.CardRace::toString(),
            'attribute' => 'nullable|string|in:'.CardAttribute::toString(),
            'ocg_date' => 'nullable|array|max:2',
            'ocg_date.before' => 'nullable|date|after_or_equal:ocg_date.after',
            'ocg_date.after' => 'nullable|date|before_or_equal:ocg_date.before',
            'tcg_date' => 'nullable|array|max:2',
            'tcg_date.before' => 'nullable|date|after_or_equal:tcg_date.after',
            'tcg_date.after' => 'nullable|date|before_or_equal:tcg_date.before'
        ];
    }
}
