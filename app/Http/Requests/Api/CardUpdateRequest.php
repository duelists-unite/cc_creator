<?php

namespace App\Http\Requests\Api;


use App\Enums\CardAttribute;
use App\Enums\CardCategory;
use App\Enums\CardDefense;
use App\Enums\CardGenre;
use App\Enums\CardRace;
use App\Enums\CardType;

class CardUpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'collection' => 'required|exists:collections,name',
            'name' => 'required|string|max:128',
            'description' => 'nullable|string',
            'strings' => 'nullable|array|max:8',
            'strings.*' => 'required|string|max:128',
            'types' => 'required|array|min:1',
            'types.*' => 'required|string|distinct|in:'.CardType::toString(),
            'atk' => 'nullable|numeric|min:0',
            'def' => 'nullable|numeric|min:0',
            'level' => 'required|numeric|min:1|max:16',
            'race' => 'required|string|in:'.CardRace::toString(),
            'attribute' => 'required|string|in:'.CardAttribute::toString(),
            'categories' => 'nullable|array|max:3',
            'categories.*' => 'required|string|distinct|in:'.CardCategory::toString(),
            'genres' => 'nullable|array|max:5',
            'genres.*' => 'required|string|distinct|in:'.CardGenre::toString(),
            'ocg_date' => 'required|date',
            'tcg_date' => 'required|date',
            'pend_scales.left' => (
                in_array(CardType::PENDULUM, request()->input('types') ?? []) ?
                    'required' :
                    'nullable'
                ) . '|numeric|min:1|max:16',
            'pend_scales.right' => (
                in_array(CardType::PENDULUM, request()->input('types') ?? []) ?
                    'required' :
                    'nullable'
                ) . '|numeric|min:1|max:16',
            'link_markers' => (
                in_array(CardType::LINK, request()->input('types') ?? []) ?
                    'required' :
                    'nullable'
                ) . '|array|size:'.request()->input('level'),
            'link_markers.*' => 'required|string|in:'.CardDefense::toString()
        ];
    }
}
