<?php

namespace App\Http\Requests\Api;

class CollectionStoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $max_file_size = 1024 * 5; // 5MB

        return [
            'collection' => "required|mimes:json|max:$max_file_size",
        ];
    }
}
