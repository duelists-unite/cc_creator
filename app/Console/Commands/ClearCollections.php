<?php

namespace App\Console\Commands;

use App\Models\Collection;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ClearCollections extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'collections:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Collection::where('available_until', '<', now())->chunk(100, function($collections) {
            foreach ($collections as $collection) {
                try {
                    if (Storage::disk('public')->exists($collection->name)) {
                        Storage::disk('public')->delete($collection->name);

                        $this->info("Removed $collection->name");
                    }

                    $collection->forceDelete();
                } catch (\Exception $exception) {

                }
            }
        });
    }
}
