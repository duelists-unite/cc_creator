<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use MeiliSearch\Client;

class UpdateSearchFilters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cards:filters';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Card filters';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new Client(config('scout.meilisearch.host'));

        $client->index('cards')->updateFilterableAttributes([
            'types',
            'atk',
            'def',
            'level',
            'race',
            'attribute',
            'categories',
            'genres',
            'ocg_date',
            'tcg_date',
            'pend_scales',
            'link_markers',
        ]);
    }
}
