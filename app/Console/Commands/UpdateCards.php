<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use App\Jobs\UpdateCardsTable;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class UpdateCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cards:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the cards table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return bool
     */
    public function handle(): bool
    {
        try {
            $name = basename(config('helper.cards.db_url'));
            $remote_hash = file_get_contents(config('helper.cards.hash_url'));

            if (
                Cache::has(config('helper.cards.hash_key')) &&
                Cache::get(config('helper.cards.hash_key')) == $remote_hash
            ) {
                return false;
            }

            Cache::put(config('helper.cards.hash_key'), $remote_hash, now()->addHour());

            $content = Http::get(config('helper.cards.db_url'));

            Storage::disk('local')->put($name, $content);

            UpdateCardsTable::dispatch();

            Log::info("Downloaded $name");

            return true;
        } catch (\Exception $exception) {
            Log::error("php artisan cards:update " . $exception->getMessage());
            Log::error($exception->getTraceAsString());

            return false;
        }
    }
}
