<?php

namespace App\Jobs;

use App\Enums\CardAttribute;
use App\Enums\CardCategory;
use App\Enums\CardDefense;
use App\Enums\CardGenre;
use App\Enums\CardLevel;
use App\Enums\CardRace;
use App\Enums\CardType;
use App\Models\Card;
use App\Models\Omega\Card as OmegaCard;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateCardsTable implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $chunk_size = 100;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Card::disableSearchSyncing();
        Card::withoutEvents(function () {

            $card_count = OmegaCard::whereIn('ot', [1, 2, 3])->count();
            $chunk_progress = 0;
            $chunk_count = ceil($card_count / $this->chunk_size);

            OmegaCard::whereIn('ot', [1, 2, 3])
                ->chunk($this->chunk_size, function ($cards) use (&$chunk_progress, $chunk_count, $card_count) {
                    foreach ($cards as $card) {
                        try {
                            DB::beginTransaction();

                            // update card information
                            Card::updateOrCreate([
                                'card_id' => $card->id
                            ], [
                                'status' => $card->ot,
                                'name' => $card->text->name,
                                'description' => $card->text->desc,
                                'strings' => $card->text->strings,
                                'types' => CardType::map($card->type),
                                'atk' => $card->atk < 0 ? 0 : $card->atk,
                                'def' => $card->def < 0 ? 0 : CardDefense::def($card->def, $card->type),
                                'level' => CardLevel::level($card->level),
                                'race' => CardRace::map($card->race),
                                'attribute' => CardAttribute::map($card->attribute),
                                'categories' => CardCategory::map($card->category),
                                'genres' => CardGenre::map($card->genre),
                                'ocg_date' => $card->ocgdate,
                                'tcg_date' => $card->tcgdate,
                                'pend_scales' => CardLevel::scales($card->level),
                                'link_markers' => CardDefense::markers($card->def, $card->type)
                            ]);

                            DB::commit();
                        } catch (\Exception $exception) {
                            Log::error("UpdateCardsTable ".$exception->getMessage());
                            Log::error($card->toArray());

                            DB::rollBack();
                        }
                    }

                    $chunk_progress++;
                    echo "Imported chunk $chunk_progress/$chunk_count of ($card_count) records\n";
            });

            // remove redundant cards
            $card_ids = OmegaCard::whereIn('ot', [1, 2, 3])->pluck('id');
            Card::whereNotIn('card_id', $card_ids)->forceDelete();
        });

        Card::enableSearchSyncing();

        // Card::flush();
        Card::all()->searchable();
    }
}
