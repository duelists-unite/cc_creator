<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CardController;
use App\Http\Controllers\DeckController;
use App\Http\Controllers\StarterController;
use App\Http\Controllers\ActionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::group([], function ($router) {

    $router->group(['prefix' => 'cards'], function ($router) {
        $router->get('/', [CardController::class, 'index'])->name('cards.index');
    });

    // $router->get('/cards', [HomeController::class, 'index'])->name('cards.index');

    $router->group(['prefix' => 'decks'], function ($router) {
        $router->get('/', [DeckController::class, 'index'])->name('decks.index');
        $router->get('create', [DeckController::class, 'create'])->name('decks.create');
        $router->post('create', [DeckController::class, 'store'])->name('decks.store');
        $router->get('view/{deck}', [DeckController::class, 'view'])->name('decks.view');
        $router->put('update/{deck}', [DeckController::class, 'update'])->name('decks.update');
        $router->delete('delete/{deck}', [DeckController::class, 'remove'])->name('decks.delete');
    });

    $router->group(['prefix' => 'bots'], function ($router) {

        $router->group(['prefix' => 'starters'], function ($router) {
            $router->get('/', [StarterController::class, 'index'])->name('starters.index');
            $router->get('create', [StarterController::class, 'create'])->name('starters.create');
            $router->post('create', [StarterController::class, 'store'])->name('starters.store');
            $router->get('view/{starter}', [StarterController::class, 'view'])->name('starters.view');
            $router->put('update/{starter}', [StarterController::class, 'update'])->name('starters.update');
            $router->delete('delete/{starter}', [StarterController::class, 'remove'])->name('starters.delete');
        });

        $router->group(['prefix' => 'actions'], function ($router) {
            $router->get('/', [ActionController::class, 'index'])->name('actions.index');
            $router->get('create', [ActionController::class, 'create'])->name('actions.create');
            $router->post('create', [ActionController::class, 'store'])->name('actions.store');
            $router->get('view/{action}', [ActionController::class, 'view'])->name('actions.view');
            $router->put('update/{action}', [ActionController::class, 'update'])->name('actions.update');
            $router->delete('delete/{action}', [ActionController::class, 'remove'])->name('actions.delete');
        });

    });
});
