<?php

use App\Http\Controllers\Api\CardController;
use App\Http\Controllers\Api\CollectionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'cards'], function($router) {

    $router->get('/', [CardController::class, 'index'])->name('card.index');
    $router->post('create', [CardController::class, 'create'])->name('card.create');
    $router->get('{id}', [CardController::class, 'view'])->name('card.view');
    $router->put('{id}/update', [CardController::class, 'update'])->name('card.update');
    $router->delete('{id}/delete', [CardController::class, 'delete'])->name('card.delete');

});

Route::group(['prefix' => 'collection'], function ($router) {

    $router->get('{id}', [CollectionController::class, 'view'])->name('collection.view');
    $router->post('create', [CollectionController::class, 'create'])->name('collection.create');
    $router->get('{id}/download', [CollectionController::class, 'download'])->name('collection.download');

});
