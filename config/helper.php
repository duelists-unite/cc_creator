<?php

return [
    'cards' => [
        'db_url' => 'https://duelistsunite.org/omega/OmegaDB.cdb',
        'hash_url' => 'https://duelistsunite.org/omega/Database.hash',
        'hash_key' => 'omegadb-hash',
        'search_page' => 6,
        'index_page' => 20
    ]
];
