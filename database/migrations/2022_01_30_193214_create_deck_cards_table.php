<?php

use App\Enums\CardLocationsInDeck;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeckCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deck_cards', function ($table) {
            $table->foreignId('deck_id')
                ->constrained('decks')
                ->onDelete('cascade');

            $table->foreignId('card_id')
                ->constrained('cards')
                ->onDelete('cascade');

            $table->enum('card_location', [CardLocationsInDeck::toArray()]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deck_cards');
    }
}
