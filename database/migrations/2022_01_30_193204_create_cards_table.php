<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function ($table) {
            $table->id();
            $table->unsignedBigInteger('card_id')->unique();
            $table->string('status', 64);
            $table->string('name', 128);
            $table->text('description');
            $table->text('strings')->nullable();
            $table->string('types', 192)->nullable();
            $table->integer('atk')->default(0);
            $table->integer('def')->default(0);
            $table->unsignedInteger('level')->default(1);
            $table->string('race', 128);
            $table->string('attribute', 128);
            $table->string('categories', 192)->nullable();
            $table->string('genres', 192)->nullable();
            $table->timestamp('ocg_date')->nullable();
            $table->timestamp('tcg_date')->nullable();
            $table->string('link_markers')->nullable();
            $table->string('pend_scales')->nullable();

            $table->timeStamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
