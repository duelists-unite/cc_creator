<div class="w-full flex flex-col gap-3">
    <form class="async-search">
        <input class="w-full p-5 rounded-full" value="{{ old('search') }}"
        type="text" name="search" placeholder="Search">
    </form>
    <div class="flex flex-wrap justify-center gap-5 min-h-full scrolling-pagination js-sortable-copy">
    </div>
</div>
