<div class="w-full flex flex-col gap-3 p-20">
    <form action="{{ route('cards.index') }}">
        <input class="w-full p-5 rounded-full" value="{{ old('search') }}"
        type="text" name="search" placeholder="Search">
    </form>
    <div class="flex flex-wrap justify-center gap-5 min-h-full scrolling-pagination">
        @if(isset($cards))
            @foreach($cards as $card)
            <div class="block p-6 max-w-sm bg-white rounded-lg border border-gray-200 shadow-md hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700 text-white">
                <div class="flex gap-2">
                    <img class="max-w-[125px] h-max" alt="{{ $card->name }}"
                    src="https://storage.googleapis.com/ygoprodeck.com/pics_artgame/{{$card->card_id}}.jpg">
                    <p class="flex-auto text-center">{{ $card->name }}</p>
                </div>
                <div class="flex-auto">
                    <p>{{ $card->description }}</p>
                </div>
            </div>
            @endforeach
            <div>
                <a class="hidden infinite--next"
                href="{{ $cards->nextPageUrl() }}{{ ($search ? "&search=$search" : "") }}"></a>
            </div>
        @endif
    </div>
</div>
