@extends('layout')

@section('content')

    <div class="grid md:grid-cols-2">
        @include('cards.partials.min-search')

        <ul class="p2 border-4 maroon border-maroon js-sortable-copy-target sortable list flex flex-column list-reset" aria-dropeffect="move"></ul>
    </div>

@endsection
