require('./bootstrap');
require('jscroll');
import sortable from 'html5sortable/dist/html5sortable.es';
const axios = require('axios').default;

sortable('.js-sortable-copy-target', {
    forcePlaceholderSize: true,
    acceptFrom: '.js-sortable-copy,.js-sortable-copy-target',
    placeholderClass: 'mb1 border border-maroon',
    maxItems: 60
});

async function getCards(search) {
  await axios.get('/cards', {
    params: {
      search: search
    }
  }).then(function (response) {
    let container = $('.scrolling-pagination');
    container.empty();

    $.each(response.data.data, function(index, card) {
      container.append(`
        <div>
          <img class="max-w-[125px] h-max" alt="${card.name}"
          src="https://storage.googleapis.com/ygoprodeck.com/pics/${card.card_id}.jpg">
        </div>
        <div>
          <a class="hidden infinite--next"
          href="{{ $cards->nextPageUrl() }}{{ ($search ? "&search=$search" : "") }}"></a>
        </div>
      `);
    });

    initInfiniteScroll('.scrolling-pagination');

    sortable('.js-sortable-copy', {
        forcePlaceholderSize: true,
        copy: true,
        acceptFrom: false,
        placeholderClass: 'mb1 bg-navy border border-yellow',
    });

  }).catch(function (error) {
    console.log(error);
  });  
}

function initInfiniteScroll(selector) {
  $(selector).jscroll({
        autoTrigger: true,
        padding: 50,
        debug: true,
        loadingHtml: '<h1 class="text-white text-center">Loading ...</h1>',
        nextSelector: '.infinite--next:last',
        contentSelector: 'div.scrolling-pagination > div',
    });
}

$(document).ready(function() {
    initInfiniteScroll('.scrolling-pagination');

    $(`.async-search input[type="text"][name="search"]`).change(() => {
      let search = $(`.async-search input[type="text"][name="search"]`).val();

      getCards(search);
    });
});
